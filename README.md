## Sistema de Cadastro

## Criando o banco de dados postgres no docker
Iremos criar um volume de dados para o docker, para que os dados fiquem persistente.<br>
`docker volume create nome_do_volume`<br>
Para visualizar se o volume foi criado faça: `docker volume list`, desta forma será 
listado o volume que foi criado.

Criando o container<br>
`docker run -d --name=oak-postgres  -p 5432:5432 -v "Volume_oak:/var/lib/postgresql/data" -e POSTGRES_PASSWORD=postgres -e PRIMARY_USER=postgres postgres
`, sempre a primeira vez vai demora um pouco, pois o docker ira no repositorio efetuar
o donwload da imagen do postgres, mais na segunda vez será coisa de 1 segundo.<br>

Para verificar se o container esta no ar faça:
`docker ps`, o sistema mostra algo assim:<br>

`CONTAINER ID   IMAGE      COMMAND                  CREATED         STATUS         PORTS                                       NAMES`<br>
`412cbbf5c104   postgres   "docker-entrypoint.s…"   3 seconds ago   Up 3 seconds   0.0.0.0:5432->5432/tcp, :::5432->5432/tcp   oak-postgres`